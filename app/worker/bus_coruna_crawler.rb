#encoding: UTF-8

BASE_URL = "http://www.coruna.es/guiaLocal/"
MAIN_URL = "http://www.coruna.es/guiaLocal/busq_lineas.jsp?current=au"

class BusCorunaCrawler
  @queue = :crawlers_queue
  
  def self.perform(route_id)  
    route = BusRoute.find(route_id)
    
    agent = Mechanize.new
    agent.user_agent = 'SW User-Agent'
    agent.open_timeout = 3
    agent.read_timeout = 4
    agent.keep_alive = false
    agent.max_history = 0
    
    page = agent.get(route.full_link)
    page.links.each do |link|
      if link.href.include? "situacion_bus.jsp"
        begin
          bus_page = link.click
          html_doc = Nokogiri::HTML(bus_page.body, 'UTF-8')
          
          route = html_doc.search("dl#linea dt").text
          route_name = html_doc.search("dl#linea dd").text
          bus_numbers = html_doc.search("dl.itemOscuro, dl.itemClaro")

          bus_numbers.each do |content|
            n = content.search("dt i").text
            hour = content.search("dd").text[/[0-9]+:[0-9]+/]
            extras = content.search("dd em")
            desc = extras[0].text if extras.size > 0
            last = extras[1].text if extras.size > 1
            nxt = extras[2].text if extras.size > 2

            if (b =  Bus.find_by_bus_number(n))            
              b.update_attribute(:last_stop, last)
              b.update_attribute(:next_stop, nxt)
              b.update_attribute(:time_to_next_stop, hour)
              b.update_attribute(:latitude, rand(30.0000..31.000))
              b.update_attribute(:longitude, rand(30.0000..31.000))
            else
              b = Bus.new(bus_number: n, 
                          description: desc, 
                          last_stop: last, 
                          next_stop: nxt, 
                          time_to_next_stop: hour, 
                          latitude: 28.990090,
                          longitude: 49.00922)
              b.bus_route = r
              b.save
            end          
          end
          
        rescue => e
          $stderr.puts "#{e.class}: #{e.message}"
          $stderr.puts e.backtrace.join("\n")
        end
      end
    end
  end
end