#encoding: UTF-8

BASE_URL = "http://www.coruna.es/guiaLocal/"
MAIN_URL = "http://www.coruna.es/guiaLocal/busq_lineas.jsp?current=au"

class CorunaCrawler
  @queue = :crawlers_queue
  
  def self.perform

    @agent = Mechanize.new
    @agent.user_agent = 'SW User-Agent'
    @agent.open_timeout = 3
    @agent.read_timeout = 4
    @agent.keep_alive = false
    @agent.max_history = 0 # reduce memory if you make lots of requests
    
    @routes = []
    
    page = @agent.get(MAIN_URL)
    html_doc = Nokogiri::HTML(page.body, 'UTF-8')
  
    html_doc.search("li.ic_bus").each do |bus_route|
      n = bus_route.search("b").text
      stops = bus_route.search("a").text.split("-")
      first = stops[0]
      last = stops[1]
      
      l = bus_route.css('a').map { |link| link['href'] }
        
      if BusRoute.find_by_route_number(n)
        r = BusRoute.find_by_route_number(n)
        
        r.update_attribute(:first_stop, first)
        r.update_attribute(:last_stop, last)
        r.update_attribute(:link, l[0])
        
        @routes << r
      else
        r = BusRoute.create(route_number: n, first_stop: first, last_stop: last, link: l[0])
        @routes << r
      end      
    end
    
    for r in @routes do
      Resque.enqueue(BusCorunaCrawler, r.id)
      Resque.enqueue(RouteStopCorunaCrawler, r.id)
    end
  end
  
end