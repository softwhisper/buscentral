#encoding: UTF-8

BASE_URL = "http://www.coruna.es/guiaLocal/"

class RouteStopCorunaCrawler
  @queue = :crawlers_queue
  
  def self.perform(route_id)
    route = BusRoute.find(route_id)
    
    agent = Mechanize.new
    agent.user_agent = 'SW User-Agent'
    agent.open_timeout = 3
    agent.read_timeout = 4
    agent.keep_alive = false
    agent.max_history = 0 # reduce memory if you make lots of requests
    
    page = agent.get(route.full_link)
    
    page.links.each do |link|
      if link.href.include? "paradas_bus2.jsp"
        begin
          bus_page = link.click
          html_doc = Nokogiri::HTML(bus_page.body, 'UTF-8')
          route_stops = html_doc.search("li.parada a")
          
          route_stops.each do |content|
            content_array = content.text.split
            n = content_array[0].to_i
            name = content_array[1..content_array.size-2].map{|s| "#{s}"}.join(' ')
            distance = content.text[/\d{1,5}m/].gsub("m","").to_i      

            if RouteStop.where(name: name, bus_route_id: route.id).size > 0 
              stop = RouteStop.where(name: name, bus_route_id: route.id).first
              
              stop.pos = n
              stop.name = name
              stop.distance = distance
              stop.bus_route = route
              
              stop.save
              
            else
              stop = RouteStop.new(pos: n, name: name, distance: distance)                              
              stop.bus_route = route
              stop.save
            end          
          end
          
        rescue => e
          $stderr.puts "#{e.class}: #{e.message}"
          $stderr.puts e.backtrace.join("\n")
        end
      end
    end
  end
end