BASE_URL = "http://www.coruna.es/guiaLocal/"

class BusRoute < ActiveRecord::Base
  has_many :buses, :class_name => "Bus"
  has_many :route_stops
  
  attr_accessible :first_stop, :last_stop, :route_number, :link
  
  def full_link
    "#{BASE_URL}#{link}"
  end
end
