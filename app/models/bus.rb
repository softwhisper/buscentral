class Bus < ActiveRecord::Base
  belongs_to :bus_route
  attr_accessible :bus_number, :description, :last_stop, :latitude, :longitude, :next_stop, :time_to_next_stop
end
