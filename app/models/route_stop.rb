class RouteStop < ActiveRecord::Base
  belongs_to :bus_route
  attr_accessible :distance, :name, :pos, :bus_route
  
  def method_missing(m)
      puts "No method #{m} creating..."
      self.class.send(:define_method, :lol) do
        puts "Method missing magix"
      end
  end
end
