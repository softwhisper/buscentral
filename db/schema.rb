# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121028180842) do

  create_table "bus_routes", :force => true do |t|
    t.string   "route_number"
    t.string   "first_stop"
    t.string   "last_stop"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "link"
  end

  create_table "buses", :force => true do |t|
    t.string   "bus_number"
    t.string   "description"
    t.string   "last_stop"
    t.string   "next_stop"
    t.string   "time_to_next_stop"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "bus_route_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  add_index "buses", ["bus_route_id"], :name => "index_buses_on_bus_route_id"

  create_table "route_stops", :force => true do |t|
    t.integer  "pos"
    t.string   "name"
    t.integer  "distance"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "bus_route_id"
  end

end
