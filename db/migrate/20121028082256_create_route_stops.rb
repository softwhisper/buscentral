class CreateRouteStops < ActiveRecord::Migration
  def change
    create_table :route_stops do |t|
      t.integer :position
      t.string :name
      t.integer :distance
      t.references :bus_route

      t.timestamps
    end
    add_index :route_stops, :bus_route_id
  end
end
