class CreateBuses < ActiveRecord::Migration
  def change
    create_table :buses do |t|
      t.string :bus_number
      t.string :description
      t.string :last_stop
      t.string :next_stop
      t.string :time_to_next_stop
      t.float :latitude
      t.float :longitude
      t.references :bus_route

      t.timestamps
    end
    add_index :buses, :bus_route_id
  end
end
