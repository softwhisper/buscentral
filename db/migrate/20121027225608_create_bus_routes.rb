class CreateBusRoutes < ActiveRecord::Migration
  def change
    create_table :bus_routes do |t|
      t.string :route_number, unique: true
      t.string :first_stop
      t.string :last_stop

      t.timestamps
    end
  end
end
