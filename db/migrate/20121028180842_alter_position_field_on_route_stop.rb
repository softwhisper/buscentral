class AlterPositionFieldOnRouteStop < ActiveRecord::Migration
  def up
    rename_column :route_stops, :position, :pos
  end

  def down
  end
end