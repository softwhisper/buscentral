class AddLinkToBusRoute < ActiveRecord::Migration
  def change
    add_column :bus_routes, :link, :string
  end
end