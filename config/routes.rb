BusCentral::Application.routes.draw do
  resources :route_stops

  resources :buses
  resources :bus_routes do
    get 'scrap', on: :collection
  end

  mount Resque::Server, :at => "/resque"
  
  root :to => 'bus_routes#index'
end
